#include "MassSpringSystemSimulator.h"

/*
** Implementation for Spring class
*/
Spring::Spring(int point0, int point1, float initialLength) {
	s_iPoint0 = point0;
	s_iPoint1 = point1;
	s_fInitialLength = initialLength;
	s_fCurrentLength = initialLength;

	// Default stiffness for Demo 1, 2, 3, 5
	s_fStiffness = 40.0f;
}

Spring::Spring(int point0, int point1, float initialLength, float stiffness) {
	s_iPoint0 = point0;
	s_iPoint1 = point1;
	s_fInitialLength = initialLength;
	s_fCurrentLength = initialLength;
	s_fStiffness = stiffness;
}

void Spring::setCurrentLength(float currentLength) {
	s_fCurrentLength = currentLength;
}

void Spring::setStiffness(float stiffness) {
	s_fStiffness = stiffness;
}

int Spring::getPoint0() {
	return s_iPoint0;
}

int Spring::getPoint1() {
	return s_iPoint1;
}

float Spring::getInitialLength() {
	return s_fInitialLength;
}

float Spring::getCurrentLength() {
	return s_fCurrentLength;
}

float Spring::getStiffness() {
	return s_fStiffness;
}


/*
** Implementation for Point class
*/
Point::Point(Vec3 position, Vec3 velocity, bool isFixed) {
	p_vPosition = position;
	p_vVelocity = velocity;
	p_bIsFixed = isFixed;
	p_vForce = Vec3(0, 0, 0);

	// Default damping factor for Demo 1, 2, 3, 5
	p_fDamping = 0.0f;
}

Point::Point(Vec3 position, Vec3 velocity, bool isFixed, float damping) {
	p_vPosition = position;
	p_vVelocity = velocity;
	p_bIsFixed = isFixed;
	p_vForce = Vec3(0, 0, 0);

	p_fDamping = damping;
}

void Point::setPosition(Vec3 position) {
	p_vPosition = position;
}

void Point::setVelocity(Vec3 velocity) {
	p_vVelocity = velocity;
}

void Point::setForce(Vec3 force) {
	p_vForce = force;
}

void Point::setIsFixed(bool isFixed) {
	p_bIsFixed = isFixed;
}

void Point::setDampingFactor(float damping) {
	p_fDamping = damping;
}

Vec3 Point::getPosition() {
	return p_vPosition;
}

Vec3 Point::getVelocity() {
	return p_vVelocity;
};

Vec3 Point::getForce() {
	return p_vForce;
}

bool Point::getIsFixed() {
	return p_bIsFixed;
}

float Point::getDampingFactor() {
	return p_fDamping;
}


/*
** Implementation for MassSpringSystemSimulator class
*/
MassSpringSystemSimulator::MassSpringSystemSimulator() {
	m_iTestCase = 0;
	m_fTimestep = -1;
	m_iTotalstep = -1;

	m_iIntegrator = 0;
	m_iUserDefinedIntegrator = 0;

	// Default width/height for cloth
	m_iclothWidth = 0;
	m_iclothHeight = 0;

	m_bGravity = true;
	m_bUserDefinedGravity = true;
	m_fDamping = 0;
	m_fUserDefinedDamping = 0;

	m_fLength = 1;
	m_fUserDefinedLength = 1;
	m_fHeight = 1;
	m_fUserDefinedHeight = 1;
	m_fWidth = 1;
	m_fUserDefinedWidth = 1;
}

void MassSpringSystemSimulator::setMass(float mass) {
	m_fMass = mass;
}

void MassSpringSystemSimulator::setStiffness(float stiffness)
{
	for (auto& s : m_lSprings)
	{
		s.setStiffness(stiffness);
	}
}

void MassSpringSystemSimulator::setDampingFactor(float damping)
{
	for (auto& p : m_lPoints)
	{
		p.setDampingFactor(damping);
	}
}

int MassSpringSystemSimulator::addMassPoint(Vec3 position, Vec3 velocity, bool isFixed) {
	Point point(position, velocity, isFixed);
	m_lPoints.push_back(point);
	return getNumberOfMassPoints() - 1;
};

void MassSpringSystemSimulator::addSpring(int masspoint1, int masspoint2, float initialLength) {
	Spring spring(masspoint1, masspoint2, initialLength);
	m_lSprings.push_back(spring);
};

int MassSpringSystemSimulator::getNumberOfMassPoints() {
	return m_lPoints.size();
};

int MassSpringSystemSimulator::getNumberOfSprings() {
	return m_lSprings.size();
};

Vec3 MassSpringSystemSimulator::getPositionOfMassPoint(int index) {
	return m_lPoints[index].getPosition();
};

Vec3 MassSpringSystemSimulator::getVelocityOfMassPoint(int index) {
	return m_lPoints[index].getVelocity();
};

void MassSpringSystemSimulator::applyExternalForce(Vec3 force) {
	m_externalForce = force;
};

void MassSpringSystemSimulator::eulerSimulation(float timestep) {
	// For Demo 1, only run once. When running unit test, the m_iTestCase = -1
	if (m_iTestCase > 0 || (m_fTimestep == -1) || m_iTotalstep < 1) {
		m_iTotalstep++;
		computeElasticForces();
		// For all points in the mass spring system, compute the velocity and position
		for (int i = 0; i < getNumberOfMassPoints(); i++) {
			Point point = m_lPoints[i];
			// newPosition = oldPosition + timestep * oldVelocity
			Vec3 newPosition = point.getPosition() + timestep * point.getVelocity();

			// Add damping
			Vec3 acceleration = (point.getForce() + m_externalForce - point.getDampingFactor() * point.getVelocity()) / m_fMass;
			// newVelocity = oldVelocity + timestep * oldAcceleration
			Vec3 newVelocity = point.getVelocity() + timestep * acceleration;

			collisionDetection(i, newPosition, newVelocity);
		}
	}
}

void MassSpringSystemSimulator::midPointSimulation(float timestep) {
	if (m_iTestCase > 0 || (m_fTimestep == -1) || m_iTotalstep < 1) {
		m_iTotalstep++;
		computeElasticForces();
		vector<Vec3> oldPositions;
		vector<Vec3> oldVelocities;

		// Mid step calculation
		for (int i = 0; i < getNumberOfMassPoints(); i++) {
			Point& point = m_lPoints[i];
			oldPositions.push_back(point.getPosition());
			oldVelocities.push_back(point.getVelocity());
			// midStepPosition = oldPosition + 1/2 * timestep * oldVel
			point.setPosition(point.getPosition() + 0.5 * timestep * point.getVelocity());

			// Add damping
			Vec3 acceleration = (point.getForce() + m_externalForce - point.getDampingFactor() * point.getVelocity()) / m_fMass;
			// midStepVelocity = oldVelocity + 1/2 * timestep * oldAcceleration
			point.setVelocity(point.getVelocity() + 0.5 * timestep * acceleration);
		}
		clearForce();

		computeElasticForces();
		// For all points in the mass spring system, compute the velocity and position using midstep result
		for (int i = 0; i < getNumberOfMassPoints(); i++) {
			Point point = m_lPoints[i];
			// newPosition = oldPosition + timestep * midStepVelocity
			Vec3 newPosition = oldPositions[i] + timestep * point.getVelocity();

			// Add damping
			Vec3 acceleration = (point.getForce() + m_externalForce - point.getDampingFactor() * oldVelocities[i]) / m_fMass;
			// newVelocity = oldVelocity + timestep * midStepAcceleration
			Vec3 newVelocity = oldVelocities[i] + timestep * acceleration;

			collisionDetection(i, newPosition, newVelocity);
		}
	}
}

void MassSpringSystemSimulator::collisionDetection(int idx, Vec3 newPosition, Vec3 newVelocity) {
	Point& point = m_lPoints[idx];
	float x = newPosition[0];
	float y = newPosition[1];
	float z = newPosition[2];

	float vx = newVelocity[0];
	float vy = newVelocity[1];
	float vz = newVelocity[2];

	float fX = m_fLength / 2;
	float fY = m_fHeight / 2;
	float fZ = m_fWidth / 2;

	if (m_iTestCase == 3) {
		if (abs(x) > fX) {
			if (x < 0) {
				x = -fX;
			}
			else {
				x = fX;
			}
			vx = -vx;
		}

		if (abs(y) > fY) {
			if (y < 0) {
				y = -fY;
			}
			else {
				y = fY;
			}
			vy = -vy;
		}

		if (abs(z) > fZ) {
			if (z < 0) {
				z = -fZ;
			}
			else {
				z = fZ;
			}
			vz = -vz;
		}
	}

	point.setPosition(Vec3(x, y, z));
	point.setVelocity(Vec3(vx, vy, vz));
	Vec3 position = point.getPosition();
	Vec3 velocity = point.getVelocity();

	std::string integration = "";
	switch (m_iIntegrator) {
	case EULER:
		integration = "Euler: ";
		break;
	case LEAPFROG:
		integration = "LeapFrog: ";
		break;
	case MIDPOINT:
		integration = "MidPoint: ";
		break;
	default:
		break;
	}
	std::cout << integration << "Point\[" + std::to_string(idx) + "\] on time step " + std::to_string(m_iTotalstep) + ": " << endl
		<< "\tPosition: " << "<" + std::to_string(x) + "," + std::to_string(y) + "," + std::to_string(z) + ">" << endl
		<< "\tVelocity: " << "<" + std::to_string(vx) + "," + std::to_string(vy) + "," + std::to_string(vz) + ">" << endl;
}

void MassSpringSystemSimulator::leapFrogSimulation(float timestep) {
	// For Demo 1, only run once. When running unit test, the m_iTestCase = -1
	if (m_iTestCase > 0 || (m_fTimestep == -1) || m_iTotalstep < 1) {
		m_iTotalstep++;
		computeElasticForces();
		// For all points in the mass spring system, compute the velocity and position
		for (int i = 0; i < getNumberOfMassPoints(); i++) {
			Point point = m_lPoints[i];
			// Add damping
			Vec3 acceleration = (point.getForce() + m_externalForce - point.getDampingFactor() * point.getVelocity()) / m_fMass;
			// newVelocity = oldVelocity + timestep * oldAcceleration
			Vec3 newVelocity = point.getVelocity() + timestep * acceleration;

			// newPosition = oldPosition + timestep * oldVelocity
			Vec3 newPosition = point.getPosition() + timestep * newVelocity;

			collisionDetection(i, newPosition, newVelocity);
		}
	}
}

void MassSpringSystemSimulator::computeElasticForces() {
	for (int i = 0; i < getNumberOfSprings(); i++) {
		Spring spring = m_lSprings[i];
		Point& p0 = m_lPoints[spring.getPoint0()];
		Point& p1 = m_lPoints[spring.getPoint1()];

		Vec3 distance = p0.getPosition() - p1.getPosition();
		float currentLength = sqrt(pow(distance[0], 2) + pow(distance[1], 2) + pow(distance[2], 2));
		spring.setCurrentLength(currentLength);
		Vec3 direction = distance / currentLength;

		Vec3 elasticForce = -(spring.getStiffness() * (currentLength - spring.getInitialLength())) * direction;

		p0.setForce(p0.getForce() + elasticForce);
		p1.setForce(p1.getForce() - elasticForce);
	}
}

void MassSpringSystemSimulator::clearForce() {
	for (int i = 0; i < getNumberOfMassPoints(); i++) {
		m_lPoints[i].setForce(Vec3(0, 0, 0));
	}
}

// UI Functions
const char* MassSpringSystemSimulator::getTestCasesStr() {
	return "Demo 1,Demo 2,Demo 3,Demo 4,Demo 5";
}

const char* MassSpringSystemSimulator::getIntegratorsStr() {
	return "Euler,LeapFrog,MidPoint";
}

void MassSpringSystemSimulator::initUI(DrawingUtilitiesClass* DUC) {
	this->DUC = DUC;
	TwType TW_TYPE_INTEGRATOR;
	switch (m_iTestCase)
	{
	case 0:
		// Simple one-step test to print on console
		TW_TYPE_INTEGRATOR = TwDefineEnumFromString("Integrator", this->getIntegratorsStr());
		TwAddVarRW(DUC->g_pTweakBar, "Integrator", TW_TYPE_INTEGRATOR, &m_iUserDefinedIntegrator, "");
		break;
	case 1:
		// Simple 2 point setup for Euler simulation
	case 2:
		// Simple 2 point setup for Midpoint simulation
		break;
	case 3:
		// Complex simulation
		TwAddVarRW(DUC->g_pTweakBar, "Timestep", TW_TYPE_FLOAT, &m_fTimestep, "step=0.0001 min=0.0001");

		TW_TYPE_INTEGRATOR = TwDefineEnumFromString("Integrator", this->getIntegratorsStr());
		TwAddVarRW(DUC->g_pTweakBar, "Integrator", TW_TYPE_INTEGRATOR, &m_iUserDefinedIntegrator, "");

		// Allow to enable/disable gravity
		TwAddVarRW(DUC->g_pTweakBar, "Gravity", TW_TYPE_BOOLCPP, &m_bUserDefinedGravity, "");

		// Allow to set global damping factor
		TwAddVarRW(DUC->g_pTweakBar, "Damping", TW_TYPE_FLOAT, &m_fUserDefinedDamping, "step=0.1 min=0");

		// Allow to customize volume
		TwAddVarRW(DUC->g_pTweakBar, "Length", TW_TYPE_FLOAT, &m_fUserDefinedLength, "step=0.2 min=1");
		TwAddVarRW(DUC->g_pTweakBar, "Height", TW_TYPE_FLOAT, &m_fUserDefinedHeight, "step=0.2 min=1");
		TwAddVarRW(DUC->g_pTweakBar, "Width", TW_TYPE_FLOAT, &m_fUserDefinedWidth, "step=0.2 min=1");

		break;
	case 4:
		// Leap frog method, simple ? complex?
		break;
	default:
		break;
	}
}

void MassSpringSystemSimulator::reset() {
	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
}

void MassSpringSystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext) {
	std::mt19937 eng;
	std::uniform_real_distribution<float> randCol(0.0f, 1.0f);

	if (m_iTestCase == 3) {
		float fX = m_fLength / 2;
		float fY = m_fHeight / 2;
		float fZ = m_fWidth / 2;
		DUC->beginLine();
		DUC->drawLine(Vec3(-fX, fY, -fZ), Vec3(1, 0, 0), Vec3(fX, fY, -fZ), Vec3(1, 0, 0));
		DUC->endLine();

		DUC->beginLine();
		DUC->drawLine(Vec3(-fX, fY, fZ), Vec3(1, 0, 0), Vec3(fX, fY, fZ), Vec3(1, 0, 0));
		DUC->endLine();

		DUC->beginLine();
		DUC->drawLine(Vec3(-fX, -fY, fZ), Vec3(1, 0, 0), Vec3(fX, -fY, fZ), Vec3(1, 0, 0));
		DUC->endLine();

		DUC->beginLine();
		DUC->drawLine(Vec3(-fX, -fY, -fZ), Vec3(1, 0, 0), Vec3(fX, -fY, -fZ), Vec3(1, 0, 0));
		DUC->endLine();

		DUC->beginLine();
		DUC->drawLine(Vec3(-fX, fY, -fZ), Vec3(0, 0, 1), Vec3(-fX, fY, fZ), Vec3(0, 0, 1));
		DUC->endLine();

		DUC->beginLine();
		DUC->drawLine(Vec3(fX, fY, -fZ), Vec3(0, 0, 1), Vec3(fX, fY, fZ), Vec3(0, 0, 1));
		DUC->endLine();

		DUC->beginLine();
		DUC->drawLine(Vec3(-fX, -fY, -fZ), Vec3(0, 0, 1), Vec3(-fX, -fY, fZ), Vec3(0, 0, 1));
		DUC->endLine();

		DUC->beginLine();
		DUC->drawLine(Vec3(fX, -fY, fZ), Vec3(0, 0, 1), Vec3(fX, -fY, -fZ), Vec3(0, 0, 1));
		DUC->endLine();

		DUC->beginLine();
		DUC->drawLine(Vec3(-fX, fY, -fZ), Vec3(0, 1, 0), Vec3(-fX, -fY, -fZ), Vec3(0, 1, 0));
		DUC->endLine();

		DUC->beginLine();
		DUC->drawLine(Vec3(-fX, fY, fZ), Vec3(0, 1, 0), Vec3(-fX, -fY, fZ), Vec3(0, 1, 0));
		DUC->endLine();

		DUC->beginLine();
		DUC->drawLine(Vec3(fX, fY, fZ), Vec3(0, 1, 0), Vec3(fX, -fY, fZ), Vec3(0, 1, 0));
		DUC->endLine();

		DUC->beginLine();
		DUC->drawLine(Vec3(fX, fY, -fZ), Vec3(0, 1, 0), Vec3(fX, -fY, -fZ), Vec3(0, 1, 0));
		DUC->endLine();
	}
	
	for (int i = 0; i < getNumberOfMassPoints(); i++) {
		DUC->setUpLighting(Vec3(), 0.4 * Vec3(1, 1, 1), 100, 0.6 * Vec3(randCol(eng), randCol(eng), randCol(eng)));
		DUC->drawSphere(m_lPoints[i].getPosition(), Vec3(0.01, 0.01, 0.01));
	}

	for (int i = 0; i < getNumberOfSprings(); i++) {
		Spring spring = m_lSprings[i];
		Vec3 position0 = m_lPoints[spring.getPoint0()].getPosition();
		Vec3 position1 = m_lPoints[spring.getPoint1()].getPosition();

		DUC->beginLine();
		DUC->drawLine(position0, Vec3(1, 0, 0), position1, Vec3(1, 0, 0));
		DUC->endLine();
	}
}

void MassSpringSystemSimulator::simpleDemoSetup() {
	setMass(10.0f);
	applyExternalForce(Vec3(0, 0, 0));
	int p0 = addMassPoint(Vec3(0.0, 0.0f, 0), Vec3(-1.0, 0.0f, 0), false);
	int p1 = addMassPoint(Vec3(0.0, 2.0f, 0), Vec3(1.0, 0.0f, 0), false);
	addSpring(p0, p1, 1.0);
}

void MassSpringSystemSimulator::complexDemoSetup() {
	setMass(10.0f);
	// In Demo3, add gravity
	if (m_bGravity) {
		applyExternalForce(Vec3(0, -9.8 * m_fMass, 0));
	}
	else {
		applyExternalForce(Vec3(0, 0, 0));
	}

	// At least ten points, at least ten springs
	// Generate 10 mass points with random initial position and velocity
	//std::mt19937 eng;
	//std::uniform_real_distribution<float> randPos(-0.5f, 0.5f);
	//for (int i = 0; i < 10; i++) {
	//	addMassPoint(Vec3(randPos(eng), randPos(eng), randPos(eng)), Vec3(0, 0, 0), false);
	//}

	//// To simplify, mannually add spring here (not random), and all initial length are equal
	//addSpring(0, 2, 0.1f);
	//addSpring(1, 3, 0.1f);
	//addSpring(2, 4, 0.1f);
	//addSpring(3, 5, 0.1f);
	//addSpring(4, 6, 0.1f);
	//addSpring(5, 7, 0.1f);
	//addSpring(6, 8, 0.1f);
	//addSpring(7, 9, 0.1f);
	//addSpring(8, 0, 0.1f);
	//addSpring(9, 1, 0.1f);

	float startHor = -((m_iclothWidth - 1) / 2.0f) * 0.1f;
	float startVer = -((m_iclothHeight - 1) / 2.0f) * 0.1f;
	float startDep = -((m_iclothHeight - 1) / 2.0f) * 0.1f;

	for (int i = 0; i < m_iclothHeight; i++) {
		for (int j = 0; j < m_iclothWidth; j++) {
			float hor = startHor + j * 0.1f;
			float ver = startVer + i * 0.1f;
			float depth = startVer + i * 0.1f;
			addMassPoint(Vec3(hor, ver,depth), Vec3(0, 0, 0), false);
		}
	}

	for (int i = 0; i < m_iclothHeight; i++) {
		for (int j = 0; j < m_iclothWidth; j++) {
			if (i < m_iclothHeight - 1) {
				addSpring(i * m_iclothWidth + j, (i + 1) * m_iclothWidth + j,0.1f);
			}

			if (j < m_iclothWidth - 1) {
				addSpring(i * m_iclothWidth + j, i * m_iclothWidth + j + 1, 0.1f);
			}

			if (i < m_iclothHeight - 1 && j < m_iclothWidth - 1) {
				addSpring(i * m_iclothWidth + j, (i + 1) * m_iclothWidth + j + 1, 0.1f);
			}

			if (i < m_iclothHeight - 1 && j >0) {
				addSpring(i * m_iclothWidth + j, (i + 1) * m_iclothWidth + j - 1, 0.09f);
			}
		}
	}
	// Apply damping
	for (int i = 0; i < m_lPoints.size(); i++) {
		m_lPoints[i].setDampingFactor(m_fDamping);
	}
}

void MassSpringSystemSimulator::notifyCaseChanged(int testCase) {
	m_iTestCase = testCase;
	m_iTotalstep = 0;
	m_lPoints.clear();
	m_lSprings.clear();
	switch (m_iTestCase)
	{
	case 0:
		cout << "Demo 1: a simple one-step test\n";
		m_fTimestep = 0.1f;
		simpleDemoSetup();
		break;
	case 1:
		cout << "Demo 2: a simple Euler simulation with 2 point setup from Demo 1\n";
		m_fTimestep = 0.005f;
		simpleDemoSetup();
		setIntegrator(EULER);
		break;
	case 2:
		cout << "Demo 3: a simple Midpoint simulation with 2 point setup from Demo 1\n";
		m_fTimestep = 0.005f;
		simpleDemoSetup();
		setIntegrator(MIDPOINT);
		break;
	case 3:
		cout << "Demo 4: complex simulation to compare the stability of integration methods\n";
		m_fTimestep = 0.001f;
		m_iclothHeight = 6;
		m_iclothWidth = 5;
		complexDemoSetup();
		// Set default integrator
		m_iUserDefinedIntegrator = EULER;
		setIntegrator(m_iUserDefinedIntegrator);
		break;
	case 4:
		cout << "Demo 5: a simple LeapFrog simulation with 2 point setup from Demo 1\n";
		m_fTimestep = 0.005f;
		simpleDemoSetup();
		setIntegrator(LEAPFROG);
		break;
	default:
		cout << "Empty Test!\n";
		break;
	}
}

void MassSpringSystemSimulator::externalForcesCalculations(float timeElapsed) {
}

void MassSpringSystemSimulator::simulateTimestep(float timestep) {
	// For all endpoints of spring, using Hook to compute and sum the internal forces
	if (m_fTimestep != -1) {
		timestep = m_fTimestep;
	}

	if (m_fTimestep != -1 && (m_iTestCase == 0 || m_iTestCase == 3) && 
	   (m_iIntegrator != m_iUserDefinedIntegrator || m_bGravity != m_bUserDefinedGravity || m_fDamping != m_fUserDefinedDamping ||
		m_fLength != m_fUserDefinedLength || m_fHeight != m_fUserDefinedHeight || m_fWidth != m_fUserDefinedWidth)) {
		// Can only manually change integrator in Demo1 and Demo4
		setIntegrator(m_iUserDefinedIntegrator);
		setGravity(m_bUserDefinedGravity);
		setDamping(m_fUserDefinedDamping);
		setLength(m_fUserDefinedLength);
		setHeight(m_fUserDefinedHeight);
		setWidth(m_fUserDefinedWidth);
		if (m_iTestCase == 0) {
			// Reset Demo1 to initial state
			m_iTotalstep = 0;
			m_lPoints.clear();
			m_lSprings.clear();
			simpleDemoSetup();
		}
		else {
			//Reset Demo4 to initial state
			m_iTotalstep = 0;
			m_lPoints.clear();
			m_lSprings.clear();
			complexDemoSetup();
		}
	}
	switch (m_iIntegrator) {
	case MIDPOINT:
		midPointSimulation(timestep);
		break;
	case LEAPFROG:
		leapFrogSimulation(timestep);
		break;
	default:
		eulerSimulation(timestep);
	}

	clearForce();
}

void MassSpringSystemSimulator::setGravity(bool gravity) {
	m_bGravity = gravity;
}

void MassSpringSystemSimulator::setDamping(float damping) {
	m_fDamping = damping;
}

void MassSpringSystemSimulator::setLength(float length) {
	m_fLength = length;
}

void MassSpringSystemSimulator::setHeight(float height) {
	m_fHeight = height;
}

void MassSpringSystemSimulator::setWidth(float width) {
	m_fWidth = width;
}

void MassSpringSystemSimulator::onClick(int x, int y) {
	m_trackmouse.x = x;
	m_trackmouse.y = y;
}

void MassSpringSystemSimulator::onMouse(int x, int y) {
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
	m_trackmouse.x = x;
	m_trackmouse.y = y;
}
