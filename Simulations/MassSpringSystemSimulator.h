#ifndef MASSSPRINGSYSTEMSIMULATOR_h
#define MASSSPRINGSYSTEMSIMULATOR_h
#include "Simulator.h"
#include <vector>

// Do Not Change
#define EULER 0
#define LEAPFROG 1
#define MIDPOINT 2
// Do Not Change

// Assume that all springs in this system has same stiffness, all points have same mass and same damping factor
class Spring {
public:
	// Constructor
	Spring(int p0, int p1, float initialLength);
	Spring(int p0, int p1, float initialLength, float stiffness);

	// Setter
	void setCurrentLength(float currentLength);
	void setStiffness(float stiffness);

	// Getter
	int getPoint0();
	int getPoint1();
	float getInitialLength();
	float getCurrentLength();
	float getStiffness();

private:
	int s_iPoint0;
	int s_iPoint1;
	float s_fInitialLength;
	float s_fCurrentLength;
	float s_fStiffness;
};

class Point
{
public:
	// Constructor
	Point(Vec3 position, Vec3 velocity, bool isFixed);
	Point(Vec3 position, Vec3 velocity, bool isFixed, float damping);

	// Setter
	void setPosition(Vec3 position);
	void setVelocity(Vec3 velocity);
	void setForce(Vec3 force);
	void setIsFixed(bool isFixed);
	void setDampingFactor(float damping);

	// Getter
	Vec3 getPosition();
	Vec3 getVelocity();
	Vec3 getForce();
	bool getIsFixed();
	float getDampingFactor();

private:
	Vec3 p_vPosition;
	Vec3 p_vVelocity;
	Vec3 p_vForce;
	bool p_bIsFixed;
	float p_fDamping;
};

class MassSpringSystemSimulator:public Simulator{
public:
	// Construtors
	MassSpringSystemSimulator();
	
	// UI Functions
	const char * getTestCasesStr();
	const char* getIntegratorsStr();
	void initUI(DrawingUtilitiesClass * DUC);
	void reset();
	void drawFrame(ID3D11DeviceContext* pd3dImmediateContext);
	void notifyCaseChanged(int testCase);
	void externalForcesCalculations(float timeElapsed);
	void simulateTimestep(float timeStep);
	void onClick(int x, int y);
	void onMouse(int x, int y);

	// Specific Functions
	void setMass(float mass);
	void setStiffness(float stiffness);
	void setDampingFactor(float damping);
	int addMassPoint(Vec3 position, Vec3 Velocity, bool isFixed);
	void addSpring(int masspoint1, int masspoint2, float initialLength);
	int getNumberOfMassPoints();
	int getNumberOfSprings();
	Vec3 getPositionOfMassPoint(int index);
	Vec3 getVelocityOfMassPoint(int index);
	void applyExternalForce(Vec3 force);
	

	// Add utilized function
	void computeElasticForces();
	void clearForce();
	void collisionDetection(int idx, Vec3 newPosition, Vec3 newVelocity);

	void eulerSimulation(float timestep);
	void midPointSimulation(float timestep);
	void leapFrogSimulation(float timestep);

	void simpleDemoSetup();
	void complexDemoSetup();
	
	void setGravity(bool gravity);
	void setDamping(float damping);

	void setLength(float length);
	void setHeight(float height);
	void setWidth(float width);

	// Do Not Change
	void setIntegrator(int integrator) {
		m_iIntegrator = integrator;
	}

private:
	// Data Attributes
	float m_fMass;
	float m_fStiffness;
	float m_fDamping;
	int m_iIntegrator;

	// UI Attributes
	Vec3 m_externalForce;
	Point2D m_mouse;
	Point2D m_trackmouse;
	Point2D m_oldtrackmouse;

	// Vector to store points in the mass spring system
	vector<Point> m_lPoints;
	// Vector to store springs in the mass spring system
	vector<Spring> m_lSprings;
	float m_fTimestep;
	int m_iTotalstep;
	int m_iUserDefinedIntegrator;
	int m_iclothWidth;
	int m_iclothHeight;

	bool m_bGravity;
	bool m_bUserDefinedGravity;
	float m_fUserDefinedDamping;

	float m_fLength;
	float m_fUserDefinedLength;
	float m_fHeight;
	float m_fUserDefinedHeight;
	float m_fWidth;
	float m_fUserDefinedWidth;
};
#endif