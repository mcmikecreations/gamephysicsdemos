#include "RigidBodySystemSimulator.h"
#include "collisionDetect.h"
#include <random>
#include <time.h>

double getRand(double max)
{
	return (double)rand() / RAND_MAX * max;
}

RigidBodySystemSimulator::RigidBodySystemSimulator()
{
}

const char* RigidBodySystemSimulator::getTestCasesStr()
{
	return "Demo 1,Demo 2,Demo 3,Demo 4";
}

void RigidBodySystemSimulator::initUI(DrawingUtilitiesClass* DUC)
{
	this->DUC = DUC;
	TwType TW_TYPE_INTEGRATOR;
	switch (m_iTestCase)
	{
	case 0:
		// Simple one-step test to print on console
		//TW_TYPE_INTEGRATOR = TwDefineEnumFromString("Integrator", this->getIntegratorsStr());
		//TwAddVarRW(DUC->g_pTweakBar, "Integrator", TW_TYPE_INTEGRATOR, &m_iUserDefinedIntegrator, "");
		break;
	case 2:
	case 3:
		TwAddVarRW(DUC->g_pTweakBar, "Gravity", TW_TYPE_BOOLCPP, &m_bHasGravity, "");
		TwAddVarRW(DUC->g_pTweakBar, "Floor", TW_TYPE_BOOLCPP, &m_bHasFloor, "");
		break;
	default:
		break;
	}
	TwAddVarRW(DUC->g_pTweakBar, "Time Factor", TW_TYPE_FLOAT, &m_fTimeFactor, "step=0.01 min=0.01");
}

void RigidBodySystemSimulator::reset()
{
	m_mouse.x = m_mouse.y = 0;
	m_trackmouse.x = m_trackmouse.y = 0;
	m_oldtrackmouse.x = m_oldtrackmouse.y = 0;
}

void RigidBodySystemSimulator::drawFrame(ID3D11DeviceContext* pd3dImmediateContext)
{
	for (auto& rb : m_rigidBodies)
	{
		if (!rb->getIsVisible()) continue;
		Box* box = dynamic_cast<Box*>(rb.get());
		if (box != nullptr)
		{
			DUC->drawRigidBody(box->getObj2World());
		}
	}
}

void RigidBodySystemSimulator::notifyCaseChanged(int testCase)
{
	m_iTestCase = testCase;
	m_fTimestep = -1;
	m_bHasGravity = false;
	m_bHasFloor = false;

	m_rigidBodies.clear();
	switch (m_iTestCase)
	{
	case 0:
	{
		cout << "Demo 1: a simple one-step test\n";
		m_fTimestep = 0.1f;
		addRigidBody(Vec3(0, 0, 0), Vec3(1, 0.6, 0.5), 2.0);
		auto* rb = getRigidBody(0);
		rb->getOrientation() = Quat(0, 0, M_PI_2);
		rb->calculateInitialParameters();
		rb->calculateInitialTensor();
		rb->applyForce(Vec3(0.3, 0.5, 0.25), Vec3(1, 1, 0));
		break;
	}
	case 1:
	{
		cout << "Demo 2: an interactive test\n";
		m_fTimestep = 0.01f;
		addRigidBody(Vec3(0, 0, 0), Vec3(1, 0.6, 0.5), 2.0);
		auto* rb = getRigidBody(0);
		rb->getOrientation() = Quat(0, 0, M_PI_2);
		rb->calculateInitialParameters();
		rb->calculateInitialTensor();
		rb->applyForce(Vec3(0.3, 0.5, 0.25), Vec3(1, 1, 0));
		break;
	}
	case 2:
	{
		cout << "Demo 3: collision test\n";
		m_fTimestep = 0.025f;
		m_dRestitutionCoeff = 1.0;

		addRigidBody(Vec3(-2, 0.5, 0), Vec3(1, 0.6, 0.5), 2.0);
		auto* rb1 = getRigidBody(0);
		rb1->getOrientation() = Quat(0, 0, 0);
		rb1->calculateInitialParameters();
		rb1->calculateInitialTensor();
		rb1->applyForce(Vec3(-2, 0.5, 0), Vec3(1, 0, 0));

		addRigidBody(Vec3(2, 1.0, 0), Vec3(1, 0.6, 0.5), 2.0);
		auto* rb2 = getRigidBody(1);
		rb2->getOrientation() = Quat(0, 0, M_PI_2);
		rb2->calculateInitialParameters();
		rb2->calculateInitialTensor();
		rb2->applyForce(Vec3(2, 1, 0), Vec3(-1, 0, 0));

		break;
	}
	case 3:
	{
		cout << "Demo 3: random test\n";
		srand(time(0));
		m_bHasGravity = false;
		m_bHasFloor = true;
		m_fTimestep = 0.025f;
		m_dRestitutionCoeff = 0.5;
		const double maxLength = 5;

		for (int i = 0; i < 6; ++i)
		{
			double sizeMax = ((i / 3.0) + 1) / 6.0;
			addRigidBody(Vec3(-maxLength / 2 + i, 3.0 - sizeMax, -maxLength / 2 + sizeMax), Vec3(sizeMax + 0.2, sizeMax + 0.5, sizeMax), sizeMax + 1);
			auto* rb = getRigidBody(i);
			rb->getOrientation() = Quat(0, 0, 0);
			rb->calculateInitialParameters();
			rb->calculateInitialTensor();
			Vec3 forcePos = Vec3(i + 1.0, 3.0 - i, i / 2.0);
			Vec3 forceDir = Vec3(i % 2 == 0 ? 1 : -1, 0, 0);
			rb->applyForce(rb->getPosition(), forceDir);
		}

		break;
	}
	}
}

void RigidBodySystemSimulator::externalForcesCalculations(float timeElapsed)
{
	if (m_iTestCase == 1 || m_iTestCase == 3)
	{
		Point2D mouseDiff;
		mouseDiff.x = m_trackmouse.x - m_oldtrackmouse.x;
		mouseDiff.y = m_trackmouse.y - m_oldtrackmouse.y;
		if (mouseDiff.x != 0 || mouseDiff.y != 0)
		{
			Mat4 worldViewInv = Mat4(DUC->g_camera.GetWorldMatrix() * DUC->g_camera.GetViewMatrix());
			worldViewInv = worldViewInv.inverse();
			Vec3 inputView = Vec3((float)mouseDiff.x, (float)-mouseDiff.y, 0);
			Vec3 inputWorld = worldViewInv.transformVectorNormal(inputView);
			// find a proper scale!
			float inputScale = 0.001f;
			inputWorld = inputWorld * inputScale;
			for (auto& rb : m_rigidBodies)
			{
				rb->applyForce(rb->getPosition() + inputWorld, inputWorld);
			}
		}
	}
}

void RigidBodySystemSimulator::simulateTimestep(float timeStep)
{
	if (m_fTimestep != -1)
	{
		timeStep = m_fTimestep;
	}
	timeStep *= m_fTimeFactor;

	// Dynamics
	for (auto& rb : m_rigidBodies)
	{
		if (rb->getMass() == -1) continue;

		ExternalForce externalForces = ExternalForce(Vec3(), Vec3());
		size_t forceCount = rb->getForceCount();

		Vec3 torque(0.0, 0.0, 0.0);
		for (size_t i = 0; i < forceCount; ++i) {
			auto& fi = rb->getForce(i);

			externalForces.m_force += fi.m_force;
			externalForces.m_position += fi.m_position;

			torque += cross(fi.m_position - rb->getPosition(), fi.m_force);
		}
		/*if (m_bHasGravity)
		{
			externalForces.m_force += m_gravity * rb->getMass();
			externalForces.m_position += rb->getPosition();
			++forceCount;
		}*/
		externalForces.m_position /= (double)forceCount;

		rb->getPosition() = rb->getPosition() + timeStep * rb->getLinearVelocity();
		rb->getLinearVelocity() += timeStep * externalForces.m_force / rb->getMass();
		if (m_bHasGravity)
		{
			rb->getLinearVelocity() += timeStep * m_gravity;
		}

		Quat oldOrientation = rb->getOrientation();
		Vec3 oldAngularVelocity = rb->getAngularVelocity();
		rb->getOrientation() = oldOrientation + timeStep / 2.0 * Quat(oldAngularVelocity.x, oldAngularVelocity.y, oldAngularVelocity.z, 0) * oldOrientation;
		rb->getOrientation() /= rb->getOrientation().norm();

		rb->getAngularMomentum() = rb->getAngularMomentum() + timeStep * torque;

		Mat4 rotationMatrix = rb->getOrientation().getRotMat();
		Mat4 rotationMatrixT = rb->getOrientation().getRotMat();
		rotationMatrixT.transpose();
		rb->getCurrentInertiaTensor() = rotationMatrix * rb->getInitialInertiaTensor() * rotationMatrixT;
		rb->getAngularVelocity() = rb->getCurrentInertiaTensor().transformVector(rb->getAngularMomentum());

		rb->clearForces();
	}

	// Collision
	for (size_t i = 0; i < m_rigidBodies.size(); ++i)
	{
		auto* rb1 = getRigidBody(i);
		double massInv1 = rb1->getMass() == -1 ? 0.0 : 1.0 / rb1->getMass();
		for (size_t j = i + 1; j < m_rigidBodies.size(); ++j)
		{
			auto* rb2 = getRigidBody(j);
			double massInv2 = rb2->getMass() == -1 ? 0.0 : 1.0 / rb2->getMass();

			CollisionInfo info = checkCollisionSAT(rb1->getObj2World(), rb2->getObj2World());
			if (info.isValid)
			{
				// Relative velocity
				Vec3 pos1 = info.collisionPointWorld - rb1->getPosition();
				Vec3 vel1 = rb1->getLinearVelocity() + cross(rb1->getAngularVelocity(), pos1);

				Vec3 pos2 = info.collisionPointWorld - rb2->getPosition();
				Vec3 vel2 = rb2->getLinearVelocity() + cross(rb2->getAngularVelocity(), pos2);

				Vec3 deltaVel = vel1 - vel2;
				double dotProd = dot(deltaVel, info.normalWorld);
				if (dotProd < 0)
				{
					// Get impulse J
					double impulse = -(1 + m_dRestitutionCoeff) * dotProd / (
						massInv1 + massInv2 + dot(
							cross(rb1->getCurrentInertiaTensor() * cross(pos1, info.normalWorld), pos1) +
							cross(rb2->getCurrentInertiaTensor() * cross(pos2, info.normalWorld), pos2)
							, info.normalWorld)
						);

					if (massInv1 != 0)
					{
						rb1->getLinearVelocity() = rb1->getLinearVelocity() + impulse * info.normalWorld * massInv1 - cross(rb1->getAngularVelocity(), pos1);
						rb1->getAngularMomentum() = rb1->getAngularMomentum() + cross(pos1, impulse * info.normalWorld);
						rb1->getAngularVelocity() = rb1->getCurrentInertiaTensor().transformVector(rb1->getAngularMomentum());
					}
					if (massInv2 != 0)
					{
						rb2->getLinearVelocity() = rb2->getLinearVelocity() - impulse * info.normalWorld * massInv2 - cross(rb2->getAngularVelocity(), pos2);
						rb2->getAngularMomentum() = rb2->getAngularMomentum() - cross(pos2, impulse * info.normalWorld);
						rb2->getAngularVelocity() = rb2->getCurrentInertiaTensor().transformVector(rb2->getAngularMomentum());
					}
				}
			}
		}

		if (m_bHasFloor)
		{
			const double floorHeight = -1.0;

			CollisionInfo info;
			info.isValid = false;
			size_t rbPointCount = rb1->getPointCount();
			for (size_t j = 0; j < rbPointCount; ++j)
			{
				Vec3 point = rb1->getLocalPointToWorld(rb1->getPointLocation(j));
				if (point.y <= floorHeight)
				{
					info.isValid = true;
					info.collisionPointWorld = point;
					info.depth = floorHeight - point.y;
					info.normalWorld = Vec3(0, 1, 0);
				}
			}

			if (info.isValid)
			{
				// Relative velocity
				Vec3 pos1 = info.collisionPointWorld - rb1->getPosition();
				Vec3 vel1 = rb1->getLinearVelocity() + cross(rb1->getAngularVelocity(), pos1);

				Vec3 deltaVel = vel1;
				double dotProd = dot(deltaVel, info.normalWorld);
				if (dotProd < 0)
				{
					// Get impulse J
					double impulse = -(1 + m_dRestitutionCoeff) * dotProd / (
						massInv1 + dot(
							cross(rb1->getCurrentInertiaTensor() * cross(pos1, info.normalWorld), pos1)
							, info.normalWorld)
						);

					if (massInv1 != 0)
					{
						rb1->getLinearVelocity() = rb1->getLinearVelocity() + impulse * info.normalWorld * massInv1 - cross(rb1->getAngularVelocity(), pos1);
						rb1->getAngularMomentum() = rb1->getAngularMomentum() + cross(pos1, impulse * info.normalWorld);
						rb1->getAngularVelocity() = rb1->getCurrentInertiaTensor().transformVector(rb1->getAngularMomentum());
					}
				}
			}
		}
	}
}

void RigidBodySystemSimulator::onClick(int x, int y)
{
	m_trackmouse.x = x;
	m_trackmouse.y = y;
}

void RigidBodySystemSimulator::onMouse(int x, int y)
{
	m_oldtrackmouse.x = x;
	m_oldtrackmouse.y = y;
	m_trackmouse.x = x;
	m_trackmouse.y = y;
}

int RigidBodySystemSimulator::getNumberOfRigidBodies()
{
	return m_rigidBodies.size();
}

Vec3 RigidBodySystemSimulator::getPositionOfRigidBody(int i)
{
	auto& rb = m_rigidBodies.at(i);
	return rb->getPosition();
}

Vec3 RigidBodySystemSimulator::getLinearVelocityOfRigidBody(int i)
{
	auto& rb = m_rigidBodies.at(i);
	return rb->getLinearVelocity();
}

Vec3 RigidBodySystemSimulator::getAngularVelocityOfRigidBody(int i)
{
	auto& rb = m_rigidBodies.at(i);
	return rb->getAngularVelocity();
}

void RigidBodySystemSimulator::applyForceOnBody(int i, Vec3 loc, Vec3 force)
{
	auto& rb = m_rigidBodies.at(i);
	rb->applyForce(loc, force);
}

void RigidBodySystemSimulator::addRigidBody(Vec3 position, Vec3 size, int mass)
{
	m_rigidBodies.push_back(std::make_shared<Box>(position, Quat(0, 0, 0), size, mass));
}

void RigidBodySystemSimulator::setOrientationOf(int i, Quat orientation)
{
	auto& rb = m_rigidBodies.at(i);
	rb->getOrientation() = orientation;
}

void RigidBodySystemSimulator::setVelocityOf(int i, Vec3 velocity)
{
	auto& rb = m_rigidBodies.at(i);
	rb->getLinearVelocity() = velocity;
}

RigidBody* RigidBodySystemSimulator::getRigidBody(int i)
{
	return m_rigidBodies.at(i).get();
}

RigidBody::RigidBody(const Vec3& initialPosition, const Quat& initialRotation, double mass)
{
	m_centerPosition = initialPosition;
	m_orientation = initialRotation;
	m_mass = mass;
	m_bIsVisible = true;
}

void Box::calculateInitialParameters()
{
	Mat4 rotation = getOrientation().getRotMat();
	Mat4 rotationT = getOrientation().getRotMat();
	rotationT.transpose();
	getCurrentInertiaTensor() = rotation * getInitialInertiaTensor() * rotationT;
	getAngularVelocity() = getCurrentInertiaTensor().transformVector(getAngularMomentum());
}

bool Box::isPointInside(Vec3 point, Vec3& outNormal)
{
	// point is initially in world space
	Mat4 transform = getObj2World();
	point = transform.inverse().transformVector(point);

	double lx = m_size.x / 2.0;
	double ly = m_size.y / 2.0;
	double lz = m_size.z / 2.0;

	double dx = 0, dy = 0, dz = 0;
	if (point.x <= lx && point.x >= 0)
	{
		dx = lx - point.x;
	}
	else if (point.x >= -lx && point.x <= 0)
	{
		dx = -lx - point.x;
	}
	if (point.y <= ly && point.y >= 0)
	{
		dy = ly - point.y;
	}
	else if (point.y >= -ly && point.y <= 0)
	{
		dy = -ly - point.y;
	}
	if (point.z <= lz && point.z >= 0)
	{
		dz = lz - point.z;
	}
	else if (point.z >= -lz && point.z <= 0)
	{
		dz = -lz - point.z;
	}
	outNormal = transform.transformVector(Vec3(dx, dy, dz));
	return dx != 0 || dy != 0 || dz != 0;
}

Box::Box(const Vec3& initialPosition, const Quat& initialRotation, const Vec3& initialSize, double mass)
	: RigidBody(initialPosition, initialRotation, mass), m_size(initialSize)
{
	double pointMass = getMass() / 8;
	double lx = m_size.x / 2.0;
	double ly = m_size.y / 2.0;
	double lz = m_size.z / 2.0;
	m_points.push_back(Vec3(lx, ly, lz));
	m_points.push_back(Vec3(lx, ly, -lz));
	m_points.push_back(Vec3(lx, -ly, lz));
	m_points.push_back(Vec3(lx, -ly, -lz));
	m_points.push_back(Vec3(-lx, ly, lz));
	m_points.push_back(Vec3(-lx, ly, -lz));
	m_points.push_back(Vec3(-lx, -ly, lz));
	m_points.push_back(Vec3(-lx, -ly, -lz));
	m_masses.push_back(pointMass);
	m_masses.push_back(pointMass);
	m_masses.push_back(pointMass);
	m_masses.push_back(pointMass);
	m_masses.push_back(pointMass);
	m_masses.push_back(pointMass);
	m_masses.push_back(pointMass);
	m_masses.push_back(pointMass);
	calculateInitialTensor();
}

void Box::calculateInitialTensor()
{
	Vec3 size = getSize();
	double lx = size.x;
	double ly = size.y;
	double lz = size.z;

	double mass = getMass();

	double ix = mass * (ly * ly + lz * lz) / 12;
	double iy = mass * (lz * lz + lx * lx) / 12;
	double iz = mass * (lx * lx + ly * ly) / 12;

	Mat4 totalInertia = Mat4(
		ix, 0.0, 0.0, 0.0,
		0.0, iy, 0.0, 0.0,
		0.0, 0.0, iz, 0.0,
		0.0, 0.0, 0.0, 1.0
	);
	m_initialInertiaTensor = totalInertia.inverse();
}

Mat4 Box::getObj2World()
{
	Vec3& size = getSize();
	Vec3& position = getPosition();

	Mat4 tScale;
	tScale.initScaling(size.x, size.y, size.z);

	Mat4 tRotate = getOrientation().getRotMat();

	Mat4 tTranslate;
	tTranslate.initTranslation(position.x, position.y, position.z);

	return tScale * tRotate * tTranslate;
}
