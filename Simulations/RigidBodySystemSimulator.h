#ifndef RIGIDBODYSYSTEMSIMULATOR_h
#define RIGIDBODYSYSTEMSIMULATOR_h
#include "Simulator.h"

//add your header for your rigid body system, for e.g.,
//#include "rigidBodySystem.h" 

#define TESTCASEUSEDTORUNTEST 2

class ExternalForce {
public:
	Vec3 m_position;
	Vec3 m_force;

	ExternalForce(const Vec3& position, const Vec3& force) : m_position(position), m_force(force) {}
};

class RigidBody {
public:
	// Avoid get/set by using non-const reference
	virtual Vec3& getPosition() { return m_centerPosition; }
	virtual Quat& getOrientation() { return m_orientation; }
	virtual Vec3& getLinearVelocity() { return m_linearVelocity; }
	virtual Vec3& getAngularVelocity() { return m_angularVelocity; }
	virtual Vec3& getAngularMomentum() { return m_angularMomentum; }
	virtual Mat4& getInitialInertiaTensor() = 0;
	virtual Mat4& getCurrentInertiaTensor() = 0;
	virtual double& getMass() { return m_mass; }
	virtual void calculateInitialParameters() = 0;
	virtual void calculateInitialTensor() = 0;

	virtual Mat4 getObj2World() = 0;

	virtual size_t getPointCount() = 0;
	virtual Vec3 getPointLocation(size_t i) = 0;
	virtual double getPointMass(size_t i) = 0;

	virtual	bool isPointInside(Vec3 point, Vec3& outNormal) = 0;
	Vec3 getLocalPointToWorld(Vec3 localPoint) { return getObj2World().transformVector(localPoint); }

	void applyForce(const Vec3& loc, const Vec3& force) { m_externalForces.push_back(ExternalForce(loc, force)); }
	size_t getForceCount() { return m_externalForces.size(); }
	ExternalForce& getForce(size_t i) { return m_externalForces.at(i); }
	void clearForces() { m_externalForces.clear(); }

	bool& getIsVisible() { return m_bIsVisible; }

	RigidBody(const Vec3& initialPosition, const Quat& initialRotation, double mass);
protected:
	Vec3 m_centerPosition, m_linearVelocity, m_angularVelocity, m_angularMomentum;
	Quat m_orientation;
	double m_mass;
	std::vector<ExternalForce> m_externalForces;
	bool m_bIsVisible;
};

class Box : public RigidBody {
public:
	/// <summary>
	/// Inversed initial inertia tensor.
	/// </summary>
	/// <returns></returns>
	Mat4& getInitialInertiaTensor() override { return m_initialInertiaTensor; }
	Mat4& getCurrentInertiaTensor() override { return m_currentInertiaTensor; }
	void calculateInitialParameters() override;
	void calculateInitialTensor() override;
	Vec3& getSize() { return m_size; }
	Mat4 getObj2World() override;

	size_t getPointCount() override { return m_points.size(); }
	Vec3 getPointLocation(size_t i) override { return m_points[i]; }
	double getPointMass(size_t i) override { return m_masses[i]; }

	bool isPointInside(Vec3 point, Vec3& outNormal) override;

	Box(const Vec3& initialPosition, const Quat& initialRotation, const Vec3& initialSize, double mass);
private:
	Vec3 m_size;
	Mat4 m_initialInertiaTensor, m_currentInertiaTensor;
	std::vector<Vec3> m_points;
	std::vector<double> m_masses;
};

class RigidBodySystemSimulator:public Simulator{
public:
	// Construtors
	RigidBodySystemSimulator();
	
	// Functions
	const char * getTestCasesStr();
	void initUI(DrawingUtilitiesClass * DUC);
	void reset();
	void drawFrame(ID3D11DeviceContext* pd3dImmediateContext);
	void notifyCaseChanged(int testCase);
	void externalForcesCalculations(float timeElapsed);
	void simulateTimestep(float timeStep);
	void onClick(int x, int y);
	void onMouse(int x, int y);

	// ExtraFunctions
	int getNumberOfRigidBodies();
	Vec3 getPositionOfRigidBody(int i);
	Vec3 getLinearVelocityOfRigidBody(int i);
	Vec3 getAngularVelocityOfRigidBody(int i);
	void applyForceOnBody(int i, Vec3 loc, Vec3 force);
	void addRigidBody(Vec3 position, Vec3 size, int mass);
	void setOrientationOf(int i,Quat orientation);
	void setVelocityOf(int i, Vec3 velocity);
	RigidBody* getRigidBody(int i);
private:
	// Attributes
	// add your RigidBodySystem data members, for e.g.,
	// RigidBodySystem * m_pRigidBodySystem; 
	std::vector<std::shared_ptr<RigidBody>> m_rigidBodies;
	float m_fTimestep = -1.0f, m_fTimeFactor = 1.0f;
	double m_dRestitutionCoeff = 1.0;
	bool m_bHasGravity = false;
	bool m_bHasFloor = false;
	Vec3 m_gravity = Vec3(0, -9.8, 0);

	// UI Attributes
	Point2D m_mouse;
	Point2D m_trackmouse;
	Point2D m_oldtrackmouse;
	};
#endif